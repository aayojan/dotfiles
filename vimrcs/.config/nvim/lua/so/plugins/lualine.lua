local opts = {}

opts.options = {
  theme = "catppuccin-mocha",
component_separators = { left = '▎', right = ' ▎'},
section_separators = { left = ' ', right = ''}
}

return opts
