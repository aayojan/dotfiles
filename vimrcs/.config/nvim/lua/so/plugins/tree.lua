local status_ok, nvimTree = pcall(require, "nvim-tree")
if not status_ok then
  return
end

local opts = {}

    opts.sort = {
      sorter = "case_sensitive",
    }
    opts.view = {
      width = 30,
    }
    opts.filters = {
      dotfiles = true,
    }
    return opts