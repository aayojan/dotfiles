local opts = {}
opts.integrations = {
    cmp = true,
    nvimtree = true,
    gitsigns = true,
    markdown = ture,
    dap = true,
    dap_ui = true,
    treesitter = true,
    treesitter_context = true,
    telescope = {
      enabled = true,
    },
    indent_blankline = {
      enabled = true,
      scope_color = "lavender", -- catppuccin color (eg. `lavender`) Default: text
      colored_indent_levels = false,
  }, 
}
opts.flavour = {
    name = "Mocha"
  }
opts.transparent_background = true
return opts