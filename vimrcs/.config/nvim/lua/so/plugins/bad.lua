local opts = {}
opts.most_splits = 3
opts.most_tabs = 3
opts.max_hjkl = 10

return opts
